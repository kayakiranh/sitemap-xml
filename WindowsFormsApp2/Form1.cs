﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
            richTextBox3.Clear();
            richTextBox2.Text += "http://www.acibadem.com.tr/hayat/," +
                "http://www.acibadem.com.tr/alzheimermerkezi/," +
                "http://www.acibadem.com.tr/bagimlilikmerkezi/," +
                "http://www.acibadem.com.tr/crohnkolitmerkezi/," +
                "http://www.acibadem.com.tr/cyberknife/," +
                "http://www.acibadem.com.tr/dbsbeyinpili/," +
                "http://www.acibadem.com.tr/endometriozismerkezi/," +
                "http://www.acibadem.com.tr/erkenteshis/," +
                "http://www.acibadem.com.tr/gammaknife/," +
                "http://www.acibadem.com.tr/gammaknife-icon/," +
                "http://www.acibadem.com.tr/hemoroidmerkezi/," +
                "http://www.acibadem.com.tr/kemikiligitransferi/," +
                "http://www.acibadem.com.tr/lenfodemmerkezi/," +
                "http://www.acibadem.com.tr/migrainetreatmentcentre/," +
                "http://www.acibadem.com.tr/migrentedavimerkezi/," +
                "http://www.acibadem.com.tr/obezitemerkezi/," +
                "http://www.acibadem.com.tr/omurgasagligimerkezi/," +
                "http://www.acibadem.com.tr/omurgaveomuriliksagligi/," +
                "http://www.acibadem.com.tr/organnaklimerkezi/," +
                "http://www.acibadem.com.tr/perinatoloji/," +
                "http://www.acibadem.com.tr/robotikcerrahi/," +
                "http://www.acibadem.com.tr/truebeam/," +
                "http://www.acibadem.com.tr/tupbebek/," +
                "http://www.acibadem.com/en/," +
                "http://www.acibademannebebek.com/," +
                "http://www.acibademdis.com.tr/," +
                "http://www.comprehensivespinecenters.com/," +
                "http://www.spineandbackhealth.com/," +
                "http://www.spineandspinalcordcenter.com/," +
                "http://www.sporcusagligimerkezi.com/," +
                "http://www.sportsmedicineturkey.com/," +
                "http://www.yatirimci.acibadem.com.tr/," +
                "http://www.acibadem.com.tr/kalpsagligimerkezleri/," +
                "http://www.acbibademgoz.com.tr/," +
                "http://www.fulyadoktorlarmerkezi.com/";                
        }

        private static List<string> linkList = new List<string>();

        private void button1_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Length <= 3)
            {
                MessageBox.Show("Doktor adlarını her kelimenin ilk harfi büyük olacak şekilde yazınız. ( Prof. Dr. Adnan Ziyagil )");
            }
            else
            {
                button1.Text = "İşlem bitene bekleyiniz...";
                button1.Enabled = false;
                richTextBox1.Enabled = false;
                richTextBox2.Enabled = false;
                richTextBox3.Clear();
                //tanımlamalar
                linkList = new List<string>();
                string[] doctorList = richTextBox1.Text.Split(',');
                string[] websiteList = richTextBox2.Text.Split(',');

                richTextBox3.AppendText("........................................................................");
                richTextBox3.AppendText(Environment.NewLine + "Arama başladı.");
                richTextBox3.AppendText(Environment.NewLine + "........................................................................");

                for (int i = 0; i < websiteList.Count(); i++) //websitelerinde dön
                {
                    string url = websiteList[i].Trim();

                    try
                    {
                        HtmlWeb hw = new HtmlWeb();
                        HtmlAgilityPack.HtmlDocument doc = hw.Load(url);
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]")) //seçili sitedeki tüm linkleri bul
                        {
                            try
                            {
                                string linkHref = link.Attributes["href"].Value;
                                string finalLink = "";

                                //link temizleme
                                string lastCharOfUrl = url.Substring(url.Length - 1, 1);
                                if (lastCharOfUrl != "/")
                                {
                                    url = url + "/";
                                }

                                string finalUrl = url + linkHref;
               
                                string[] finalUrlArray = finalUrl.Split('/').Distinct().ToArray();
                                for (int x = 0; x < finalUrlArray.Count(); x++)
                                {
                                    if (x == 0)
                                    {
                                        finalLink += finalUrlArray[x] + "//";
                                    }
                                    else
                                    {
                                        if (finalUrlArray[x] != "")
                                        {
                                            finalLink += finalUrlArray[x] + "/";
                                        }
                                    }
                                }

                                if (finalLink.Contains(".asp"))
                                {
                                    finalLink = finalLink.Remove(finalLink.Length - 1);
                                }

                                //link kontrolü
                                if (!linkList.Contains(finalLink) && linkHref != "/" && linkHref != "#" && linkHref != "" && !linkHref.Contains("http") && !linkHref.Contains("https") && !linkHref.Contains("javascript"))
                                {
                                    linkList.Add(finalLink);

                                    try
                                    {
                                        HtmlWeb web2 = new HtmlWeb();
                                        HtmlAgilityPack.HtmlDocument doc2 = web2.Load(finalLink);
                                        for (int k = 0; k < doctorList.Count(); k++) //seçili sayfada tüm doktor isimlerini ara
                                        {
                                            byte[] bytes = Encoding.UTF8.GetBytes(doc2.DocumentNode.InnerText);
                                            string utf8_String = Encoding.UTF8.GetString(bytes);
                                            string utf8_String_Lower = Encoding.UTF8.GetString(bytes).ToLower();

                                            byte[] bytes2 = Encoding.UTF8.GetBytes(doc2.DocumentNode.InnerHtml);
                                            string utf8_String2 = Encoding.UTF8.GetString(bytes2);
                                            string utf8_String2_Lower = Encoding.UTF8.GetString(bytes2).ToLower();

                                            byte[] bytes3 = Encoding.UTF8.GetBytes(doc2.DocumentNode.OuterHtml);
                                            string utf8_String3 = Encoding.UTF8.GetString(bytes3);
                                            string utf8_String3_Lower = Encoding.UTF8.GetString(bytes3).ToLower();

                                            if (utf8_String.ToString().Contains(doctorList[k].Trim()) || utf8_String_Lower.ToString().Contains(doctorList[k].Trim().ToLower()) || utf8_String2.ToString().Contains(doctorList[k].Trim()) || utf8_String2_Lower.ToString().Contains(doctorList[k].Trim().ToLower()) || utf8_String3.ToString().Contains(doctorList[k].Trim()) || utf8_String3_Lower.ToString().Contains(doctorList[k].Trim().ToLower())) //doktor bulunursa listeye ekle
                                            {
                                                richTextBox3.AppendText(Environment.NewLine + "doktor = " + doctorList[k] + " , sayfa = " + finalLink);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        richTextBox3.AppendText(Environment.NewLine + "hata_t1 = " + ex.Message + " , sayfa = " + finalLink);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                richTextBox3.AppendText(Environment.NewLine + "hata_t2 = " + ex.Message + " , sayfa = " + url + link.Attributes["href"].Value);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        richTextBox3.AppendText(Environment.NewLine + "hata_t3 = " + ex.Message + " , sayfa = " + url);
                    }
                }

                richTextBox3.AppendText(Environment.NewLine + "........................................................................");
                richTextBox3.AppendText(Environment.NewLine + "Arama tamamlandı. " + linkList.Count().ToString() + " adet link tarandı.");
                richTextBox3.AppendText(Environment.NewLine + "........................................................................");

                button1.Text = "Ara";
                button1.Enabled = true;
                richTextBox1.Enabled = true;
                richTextBox2.Enabled = true;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
            Environment.Exit(1);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            Environment.Exit(1);
        }
    }
}