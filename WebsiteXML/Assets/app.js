﻿$("#startXml").click(function() {

    $('#loading').css("display", "block");
    $('#Result').css("display", "block");
    $('#startXml').css("pointer-events", "none");
    $("#SiteName").prop('disabled', true);

    $.ajax({
        url: '/CreateXML',
        data: JSON.stringify({ 'siteName': $('#siteName').val() }),
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            $('#Result').css("display", "block");
            $('#Count').css("display", "block");
            $('#createXml').css("display", "block");
            $('#loading').css("display", "none");
            $('#Result').val(response);
            try {
                $('#Count').val($("#Result").val().match(/\burl\b/g).length / 2);
            } catch (e) {
                $('#Count').val("0");
            }
            $('#startXml').css("pointer-events", "auto");
            $("#SiteName").prop('disabled', false);
        },
        failure: function(response) {
            $('#Result').css("display", "block");
            $('#loading').css("display", "none");
            $('#Result').val(response);
            $('#Count').val("0");
            $('#startXml').css("pointer-events", "auto");
            $("#SiteName").prop('disabled', false);
        }
    });
});

$("#createXml").click(function() {
    var url = '/ExportXML';
    window.open(url);
});