﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using HtmlAgilityPack;

namespace WebsiteXML.Controllers
{
    [RoutePrefix("")]
    public class DefaultController : Controller
    {
        private static List<string> _linkList = new List<string>();
        private static List<string> _linkList2 = new List<string>();
        private static List<string> _linkList3 = new List<string>();
        private static List<string> _linkList4 = new List<string>();
        private static List<string> _linkList5 = new List<string>();
        private static List<string> _assetList = new List<string>();

        [Route]
        public ActionResult Index()
        {
            return View();
        }

        [Route("CreateXML")]
        [HttpPost]
        public JsonResult CreateXML(string siteName)
        {
            try
            {
                Session["xml"] = "";

                //link boş ise
                if (string.IsNullOrWhiteSpace(siteName)) return Json("Geçersiz site linki");

                //geçersiz link ise
                if (!Uri.IsWellFormedUriString(siteName, UriKind.Absolute)) return Json("Geçersiz site linki");

                _linkList = new List<string>();
                _linkList2 = new List<string>();
                _linkList3 = new List<string>();
                _linkList4 = new List<string>();
                _linkList5 = new List<string>();

                //domain tanıma
                var parts = siteName.Split('/');

                var basicLink = parts[0] + "//" + parts[1] + parts[2];

                var xmlDoc = new XmlDocument();
                XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                xmlDoc.AppendChild(docNode);

                XmlNode productsNode = xmlDoc.CreateElement("urlset");
                xmlDoc.AppendChild(productsNode);

                #region 1. TARAMA

                var url = siteName;
                var hw = new HtmlWeb();
                var doc = hw.Load(url);

                var l1 = PageList(doc, url, siteName);

                if (l1.Count == 0)
                {
                    XmlNode nameNode = xmlDoc.CreateElement("url");
                    nameNode.AppendChild(xmlDoc.CreateTextNode(siteName));
                    productsNode.AppendChild(nameNode);

                    Session["xml"] = xmlDoc.InnerXml;

                    return Json(xmlDoc.InnerXml);
                }

                if (l1.Count == 1)
                {
                    if (l1[0] == siteName)
                    {
                        XmlNode nameNode = xmlDoc.CreateElement("url");
                        nameNode.AppendChild(xmlDoc.CreateTextNode(siteName));
                        productsNode.AppendChild(nameNode);

                        Session["xml"] = xmlDoc.InnerXml;

                        return Json(xmlDoc.InnerXml);
                    }
                    else
                    {
                        XmlNode nameNode = xmlDoc.CreateElement("url");
                        nameNode.AppendChild(xmlDoc.CreateTextNode(siteName));
                        productsNode.AppendChild(nameNode);

                        XmlNode nameNode2 = xmlDoc.CreateElement("url");
                        nameNode2.AppendChild(xmlDoc.CreateTextNode(l1[0]));
                        productsNode.AppendChild(nameNode2);

                        Session["xml"] = xmlDoc.InnerXml;

                        return Json(xmlDoc.InnerXml);
                    }
                }

                foreach (var link in l1)
                {
                    var finalLink = UrlCleaner(basicLink, "", link, siteName);

                    if (!_linkList.Contains(finalLink) &&
                        finalLink.Contains(siteName)
                    )
                    {
                        XmlNode nameNode = xmlDoc.CreateElement("url");
                        nameNode.AppendChild(xmlDoc.CreateTextNode(finalLink));
                        productsNode.AppendChild(nameNode);

                        _linkList.Add(finalLink);
                    }
                }

                DownloadHtml(_linkList);

                #endregion

                #region 2. TARAMA

                foreach (var item in _linkList)
                {
                    var hw2 = new HtmlWeb();
                    var doc2 = hw2.Load(item);

                    var l2 = PageList(doc2, item, siteName);

                    foreach (var link in l2)
                    {
                        var finalLink = UrlCleaner(basicLink, item, link, siteName);

                        if (!_linkList.Contains(finalLink) &&
                            !_linkList2.Contains(finalLink) &&
                            finalLink.Contains(siteName)
                        )
                        {
                            XmlNode nameNode = xmlDoc.CreateElement("url");
                            nameNode.AppendChild(xmlDoc.CreateTextNode(finalLink));
                            productsNode.AppendChild(nameNode);

                            _linkList2.Add(finalLink);
                        }
                    }
                }

                DownloadHtml(_linkList2);

                #endregion

                #region 3. TARAMA

                foreach (var item in _linkList2)
                {
                    var hw3 = new HtmlWeb();
                    var doc3 = hw3.Load(item);

                    var l3 = PageList(doc3, item, siteName);

                    foreach (var link in l3)
                    {
                        var finalLink = UrlCleaner(basicLink, item, link, siteName);

                        if (!_linkList.Contains(finalLink) &&
                            !_linkList2.Contains(finalLink) &&
                            !_linkList3.Contains(finalLink) &&
                            finalLink.Contains(siteName)
                        )
                        {
                            XmlNode nameNode = xmlDoc.CreateElement("url");
                            nameNode.AppendChild(xmlDoc.CreateTextNode(finalLink));
                            productsNode.AppendChild(nameNode);

                            _linkList3.Add(finalLink);
                        }
                    }
                }

                DownloadHtml(_linkList3);

                #endregion

                #region 4. TARAMA

                foreach (var item in _linkList3)
                {
                    var hw4 = new HtmlWeb();
                    var doc4 = hw4.Load(item);

                    var l4 = PageList(doc4, item, siteName);

                    foreach (var link in l4)
                    {
                        var finalLink = UrlCleaner(basicLink, item, link, siteName);

                        if (!_linkList.Contains(finalLink) &&
                            !_linkList2.Contains(finalLink) &&
                            !_linkList3.Contains(finalLink) &&
                            !_linkList4.Contains(finalLink) &&
                            finalLink.Contains(siteName)
                        )
                        {
                            XmlNode nameNode = xmlDoc.CreateElement("url");
                            nameNode.AppendChild(xmlDoc.CreateTextNode(finalLink));
                            productsNode.AppendChild(nameNode);

                            _linkList4.Add(finalLink);
                        }
                    }
                }

                DownloadHtml(_linkList4);

                #endregion

                #region 5. TARAMA

                foreach (var item in _linkList4)
                {
                    var hw5 = new HtmlWeb();
                    var doc5 = hw5.Load(item);

                    var l5 = PageList(doc5, item, siteName);

                    foreach (var link in l5)
                    {
                        var finalLink = UrlCleaner(basicLink, item, link, siteName);

                        if (!_linkList.Contains(finalLink) &&
                            !_linkList2.Contains(finalLink) &&
                            !_linkList3.Contains(finalLink) &&
                            !_linkList4.Contains(finalLink) &&
                            !_linkList5.Contains(finalLink) &&
                            finalLink.Contains(siteName)
                        )
                        {
                            XmlNode nameNode = xmlDoc.CreateElement("url");
                            nameNode.AppendChild(xmlDoc.CreateTextNode(finalLink));
                            productsNode.AppendChild(nameNode);

                            _linkList5.Add(finalLink);
                        }
                    }
                }

                DownloadHtml(_linkList5);

                #endregion


                DownloadAssets();

                #region TEMİZLE

                foreach (XmlNode node in xmlDoc.SelectNodes("//url"))
                {
                    var nodeValue = node.InnerText;

                    if (nodeValue.Length > 1)
                        if (nodeValue.Substring(nodeValue.Length - 1, 1) == "/")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 1);

                            foreach (XmlNode node2 in xmlDoc.SelectNodes("//url"))
                                if (node2.InnerText == newNodeValue)
                                    xmlDoc.DocumentElement.RemoveChild(node2);
                        }

                    if (nodeValue.Length > 11 && nodeValue.Contains("/index.html"))
                        if (nodeValue.Substring(nodeValue.Length - 11) == "/index.html")
                        {
                            var newNodeValue = nodeValue.Replace("/index.html", "/");

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }


                    if (nodeValue.Length > 13 && nodeValue.Contains("/default.html"))
                        if (nodeValue.Substring(nodeValue.Length - 13) == "/default.html")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 12);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }


                    if (nodeValue.Length > 10 && nodeValue.Contains("/index.asp"))
                        if (nodeValue.Substring(nodeValue.Length - 10) == "/index.asp")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 9);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }


                    if (nodeValue.Length > 12 && nodeValue.Contains("/default.asp"))
                        if (nodeValue.Substring(nodeValue.Length - 12) == "/default.asp")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 11);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }


                    if (nodeValue.Length > 10 && nodeValue.Contains("/index.php"))
                        if (nodeValue.Substring(nodeValue.Length - 10) == "/index.php")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 9);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }


                    if (nodeValue.Length > 12 && nodeValue.Contains("/default.php"))
                        if (nodeValue.Substring(nodeValue.Length - 12) == "/default.php")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 11);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }

                    if (nodeValue.Length > 11 && nodeValue.Contains("/index.aspx"))
                        if (nodeValue.Substring(nodeValue.Length - 11) == "/index.aspx")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 10);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }


                    if (nodeValue.Length > 13 && nodeValue.Contains("/default.aspx"))
                        if (nodeValue.Substring(nodeValue.Length - 13) == "/default.aspx")
                        {
                            var newNodeValue = nodeValue.Remove(nodeValue.Length - 12);

                            NodeUpdate(xmlDoc, nodeValue, newNodeValue);
                        }
                }

                #endregion

                Session["xml"] = xmlDoc.InnerXml;

                return Json(xmlDoc.InnerXml);
            }
            catch (Exception e)
            {
                return Json("Hata : " + e.Message);
            }
        }

        private void DownloadHtml(List<string> linkList)
        {
            foreach (var item in linkList)
            {
                var hw = new HtmlWeb();
                var doc = hw.Load(item);

                var splits = item.Split('/');
                string[] splits2 = splits.Distinct().ToArray();
                var pageName = splits2[splits2.Length - 1] == "" ? "index" : splits2[splits2.Length - 1];
                var filePath = Server.MapPath("~/Uploads/" + pageName + ".html");
                FileStream fs1 = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                writer.Write(doc.DocumentNode.InnerHtml);
                writer.Close();

            }
        }

        [Route("ExportXML")]
        [ValidateInput(false)]
        public ActionResult GenerateXML()
        {
            if (Session["xml"] != null)
            {
                if (Session["xml"].ToString() != "")
                {
                    var xt = Session["xml"].ToString();
                    var doc = new XmlDocument();
                    doc.LoadXml(xt);

                    Response.ClearContent();
                    Response.Charset = "utf-8";
                    Response.AddHeader("content-disposition", "attachment; filename=sitemap.xml");

                    Response.ContentType = "application/xml";
                    var sw = new StringWriter();
                    sw.Write(xt);
                    Response.Write(sw.ToString());
                    Response.End();
                }
            }

            return View();
        }

        public string UrlCleaner(string basic, string basic2, string link, string siteName)
        {
            if (link != "")
            {
                var finalLink = "";

                if (link.Contains("../"))
                {
                    var linkPart1 = link.Split('/')[1];

                    if (!linkPart1.Contains(".php") &&
                        !linkPart1.Contains(".asp") &&
                        !linkPart1.Contains(".aspx") &&
                        !linkPart1.Contains(".html") &&
                        linkPart1 != ""
                    )
                        finalLink = siteName + linkPart1 + "/";
                    else
                        finalLink = siteName + linkPart1;
                }
                else
                {
                    if (link.Contains("./")) link = link.Replace("./", "/");

                    if (link.Substring(0, 1) != "/") link = "/" + link;

                    if (siteName.Split('/').Length >= 3)
                        if (link.Contains(siteName.Split('/')[3]))
                            link = link.Replace("/" + siteName.Split('/')[3] + "/", "");

                    if (finalLink.Contains(siteName))
                        finalLink = link;
                    else
                        finalLink = siteName + link;

                    finalLink = finalLink.Replace("//", "/");
                    finalLink = finalLink.Replace("https:/", "https://");
                    finalLink = finalLink.Replace("http:/", "http://");
                }

                finalLink = finalLink.Replace(" ", "").ToLower();

                if (!finalLink.Contains(".php") &&
                    !finalLink.Contains(".asp") &&
                    !finalLink.Contains(".aspx") &&
                    !finalLink.Contains(".html") &&
                    finalLink.Substring(finalLink.Length - 1, 1) != "/"
                )
                {
                    finalLink = finalLink + "/";
                }

                    if (finalLink.Contains("/#") || finalLink.Contains("/.")) return siteName;

                return finalLink;
            }

            return siteName;
        }

        public void NodeUpdate(XmlDocument xmlDoc, string nodeValue, string newNodeValue)
        {
            var counter0 = 0;
            foreach (XmlNode node2 in xmlDoc.SelectNodes("//url"))
                if (node2.InnerText == nodeValue)
                {
                    counter0++;
                    node2.InnerText = newNodeValue;
                }

            if (counter0 >= 1)
            {
                var counter = 0;
                foreach (XmlNode node2 in xmlDoc.SelectNodes("//url"))
                    if (node2.InnerText == newNodeValue)
                    {
                        counter++;

                        if (counter > 1) xmlDoc.DocumentElement.RemoveChild(node2);
                    }

                var counter2 = 0;
                foreach (XmlNode node2 in xmlDoc.SelectNodes("//url"))
                    if (node2.InnerText == nodeValue)
                    {
                        counter2++;

                        if (counter2 > 1) xmlDoc.DocumentElement.RemoveChild(node2);
                    }
            }
        }

        public List<string> PageList(HtmlDocument doc, string url, string siteName)
        {
            var cleanList = new List<string>();

            try
            {
                if (doc.DocumentNode.InnerHtml.Contains("http-equiv=\"refresh\""))
                {
                    var pFrom = doc.DocumentNode.InnerHtml.IndexOf("url=") + "url=".Length;
                    var pTo = doc.DocumentNode.InnerHtml.LastIndexOf("\">");

                    var redirectedLink = doc.DocumentNode.InnerHtml.Substring(pFrom, pTo - pFrom);
                    var hw2 = new HtmlWeb();
                    var doc2 = hw2.Load(redirectedLink);

                    PageList(doc2, redirectedLink,"");
                }
                else
                {
                    #region LİNK TARAMA

                    foreach (var item in doc.DocumentNode.SelectNodes("//a[@href]"))
                    {
                        var l1ItemValue = item.Attributes["href"].Value;

                        if (l1ItemValue != "/" &&
                            l1ItemValue != "#" &&
                            l1ItemValue != "" &&
                            l1ItemValue.Substring(0, 1) != "#" &&
                            !l1ItemValue.Contains("javascript") &&
                            !l1ItemValue.Contains("http") &&
                            !l1ItemValue.Contains("tel:") &&
                            !l1ItemValue.Contains("mail:") &&
                            !l1ItemValue.Contains("mailto:") &&
                            !l1ItemValue.Contains(".css") &&
                            !l1ItemValue.Contains(".CSS") &&
                            !l1ItemValue.Contains(".js") &&
                            !l1ItemValue.Contains(".JS") &&
                            !l1ItemValue.Contains(".jpg") &&
                            !l1ItemValue.Contains(".JPG") &&
                            !l1ItemValue.Contains(".jpeg") &&
                            !l1ItemValue.Contains(".JPEG") &&
                            !l1ItemValue.Contains(".png") &&
                            !l1ItemValue.Contains(".PNG") &&
                            !l1ItemValue.Contains(".gif") &&
                            !l1ItemValue.Contains(".GIF") &&
                            !l1ItemValue.Contains(".pdf") &&
                            !l1ItemValue.Contains(".PDF") &&
                            !l1ItemValue.Contains(".cshtml") &&
                            !l1ItemValue.Contains("ftp:") &&
                            !l1ItemValue.Contains("file:") &&
                            l1ItemValue.Split(new[] {"http"}, StringSplitOptions.None).Length <= 2
                        )
                            cleanList.Add(l1ItemValue);
                    }

                    #endregion

                    #region EK TARAMALAR

                    foreach (var item in doc.DocumentNode.SelectNodes("//link[@href]"))
                    {
                        var l1ItemValue = item.Attributes["href"].Value;

                        var finalAssetLink = siteName + l1ItemValue;

                        finalAssetLink = finalAssetLink.Replace("//", "/");
                        string[] finalAssetLinkArray = finalAssetLink.Split('/');
                        string[] finalAssetLinkArray2 = finalAssetLinkArray.Distinct().ToArray();
                        finalAssetLink = "";
                        for (int i = 0; i < finalAssetLinkArray2.Length; i++)
                        {
                            if (i == 0)
                            {
                                finalAssetLink += finalAssetLinkArray2[i] + "//";
                            }
                            else if (i == finalAssetLinkArray2.Length - 1)
                            {
                                finalAssetLink += finalAssetLinkArray2[i];
                            }
                            else
                            {
                                finalAssetLink += finalAssetLinkArray2[i] + "/";
                            }
                        }

                        if (l1ItemValue.Contains(".css") && !_assetList.Contains(finalAssetLink))
                        {
                            if (!l1ItemValue.Contains("//"))
                            {
                                _assetList.Add(finalAssetLink);
                            }                            
                        }
                    }

                    foreach (var item in doc.DocumentNode.SelectNodes("//script[@src]"))
                    {
                        var l1ItemValue = item.Attributes["src"].Value;

                        var finalAssetLink = siteName + l1ItemValue;

                        finalAssetLink = finalAssetLink.Replace("//", "/");
                        string[] finalAssetLinkArray = finalAssetLink.Split('/');
                        string[] finalAssetLinkArray2 = finalAssetLinkArray.Distinct().ToArray();
                        finalAssetLink = "";
                        for (int i = 0; i < finalAssetLinkArray2.Length; i++)
                        {
                            if (i == 0)
                            {
                                finalAssetLink += finalAssetLinkArray2[i] + "//";
                            }
                            else if (i == finalAssetLinkArray2.Length - 1)
                            {
                                finalAssetLink += finalAssetLinkArray2[i];
                            }
                            else
                            {
                                finalAssetLink += finalAssetLinkArray2[i] + "/";
                            }
                        }

                        if ((l1ItemValue.Contains(".json") || l1ItemValue.Contains(".js")) && !_assetList.Contains(finalAssetLink))
                        {
                            if (!l1ItemValue.Contains("//"))
                            {
                                _assetList.Add(finalAssetLink);
                            }
                        }
                    }

                    #endregion

                }
            }
            catch { }

            return cleanList;
        }

        public void DownloadAssets()
        {
            foreach (var item in _assetList)
            {
                var result = string.Empty;
                using (var webClient = new System.Net.WebClient())
                {
                    result = webClient.DownloadString(item);
                }

                var splits = item.Split('/');
                var filePath = Server.MapPath("~/Uploads/" + splits[splits.Length - 1]);
                FileStream fs1 = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter writer = new StreamWriter(fs1);
                writer.Write(result);
                writer.Close();
            }
        }

    }
}